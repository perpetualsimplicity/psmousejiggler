function get-jiggywithit {
	
	param($minutes=10)

	<#
        .SYNOPSIS
        "Mouse Jiggler"

        .DESCRIPTION
        Functions like a mouse jiggler by sending keyboard input to keep a system alive.
        Takes a parameter of minutes to run.

        .PARAMETER Minutes
        Total minutes the jiggler should run.

        
        .INPUTS
        None. You cannot pipe objects to get-jiggywithit.

        .OUTPUTS
        None. This is a silent running script.

        .EXAMPLE
        C:\PS> get-jiggywithit.ps1 -minutes 5
        File.txt

    #>

	$myshell = New-Object -com "Wscript.Shell"

	for ($i = 0; $i -lt $minutes; $i++) {
		Start-Sleep -Seconds 60
		$myshell.sendkeys("^")
	}
}